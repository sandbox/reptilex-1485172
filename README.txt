This is a module created at ecologic.eu by Sebastian Ossio
the purpose was to assign one user a specific node and give
permissions to edit it even though he has no global permissions for it.

The way this works is the following:
 User A has rights to edit node X.
 User B has no rights to edit node X.

 User A edits node X and assignes through the node_assignment interface at the bottom of each node
 node X to node B and can add a text optionally "Please check this and assign it back to me"
 
 User B is emailed with the message included, telling him he can edit the node.
 
 When the User B tries to edit the node now he has full access to edit it. He can also "reassign"
 and use the node_assignment form.
 
 
 This module is very basic by design, it MIGHT get extended to 
 1. assign all translations of the node when assigning one language.
 2. an administration page to define which content types can be assigned and not enable it for all as it is now 
 3. extend the administration page to define wether the translation should automatically assigned too.
 4. allow translation string with t()
 
 This extensions and others depend on the interest and our time and use for it. Or wether someone else
 is willing to step in.
 
 Enjoy